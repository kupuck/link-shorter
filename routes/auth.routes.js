const { Router } = require('express');
const config = require('config');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const User = require('../models/user');
const router = Router();

// /api/signup
router.post(
  '/signup',
  [
    check('email', 'Incorrect email').isEmail(),
    check('password', 'Incorrect password').isLength({ min: 6})
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Incorrect parameters'
        });
      }
      const  { email, password } = req.body;
      const candidate = await User.findOne({email});
      if (candidate) {
        res.status(400).json({ message: 'User already exists' });
      }
      const hashedPassword = await bcrypt.hash(password, 12);
      const user = new User({ email, password: hashedPassword });
      await user.save();
      res.status(200).json({ message: 'User successfully created' });
    } catch (e) {
      res.status(500).json({ message: 'Server Error' })
    }
  });

// /api/signin
router.post(
  '/signin',
  [
    check('email', 'Incorrect email').normalizeEmail().isEmail(),
    check('password', 'Incorrect password').exists()
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Incorrect parameters'
        });
      }
      const  { email, password } = req.body;
      const user = await User.findOne({email});
      if (user) {
        const isMatch = await bcrypt.compare(password, user.password);
        if (isMatch) {
          const token = jwt.sign(
            { userId: user.id },
            config.get('jwtSecret'),
            { expiresIn: '1h' }
          );
          res.json({token, userId: user.id});
        } else {
          return res.status(400).json({ message: 'Incorrect credentials' });
        }
      } else {
        return res.status(400).json({ message: 'User not found' });
      }
    } catch (e) {
      res.status(500).json({ message: 'Server Error' })
    }
  });

module.exports = router;