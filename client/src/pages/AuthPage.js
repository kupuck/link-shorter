import React, { useState, useEffect, useContext } from 'react';
import useHttp from '../hooks/http.hook';
import useMessage from '../hooks/message.hook';
import AuthContext from '../context/AuthContext';

const AuthPage = () => {
  const message = useMessage();
  const auth = useContext(AuthContext);
  const { loading, error, request, clearError } = useHttp();
  const [form, setForm] = useState({ email: '', password: '' });

  useEffect(() => {
    message(error);
    clearError();
  }, [error, message, clearError]);

  useEffect(() => {
    window.M.updateTextFields();
  }, []);

  const changeHandle = event => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  const signIn = async () => {
    try {
      const data = await request('/api/auth/signin', 'POST', {...form});
      auth.signIn(data.token, data.userId);
    } catch (e) {
      
    }
  };

  const signUp = async () => {
    try {
      const data = await request('/api/auth/signup', 'POST', {...form});
      message(data.message);
    } catch (e) {
      
    }
  };

  return (
    <div className="row">
      <div className="col s6 offset-s3">
        <h1>Shorten your link</h1>
        <div className="card blue darken-1">
          <div className="card-content white-text">
            <span className="card-title">Sign In</span>
            <div className="input-field">
              <input className="yellow-input" type="email" name="email" id="email" placeholder="Enter your email" value={form.email} onChange={changeHandle} />
              <label htmlFor="email">Email</label>
            </div>
            <div className="input-field">
              <input className="yellow-input" type="password" name="password" id="password" placeholder="Enter your password" value={form.password} onChange={changeHandle} />
              <label htmlFor="password">Password</label>
            </div>
          </div>
          <div className="card-action">
            <button className="btn yellow darken-4" onClick={signIn} disabled={loading}>Sign In</button>
            <button className="btn grey lighten-4 black-text" onClick={signUp} disabled={loading}>Sign Up</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuthPage;