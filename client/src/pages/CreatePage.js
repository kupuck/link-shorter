import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext from '../context/AuthContext';
import useHttp from '../hooks/http.hook';

const CreatePage = () => {
  const history = useHistory();
  const { request } = useHttp();
  const [link, setLink] = useState('');
  const auth = useContext(AuthContext);

  useEffect(() => {
    window.M.updateTextFields();
  }, []);

  const pressHandler = async event => {
    if (event.key === 'Enter') {
      try {
        const data = await request(
          '/api/link/generate',
          'POST',
          { from: link },
          { Authorization: `Bearer ${auth.token}`
        });
        history.push(`/detail/${data.link._id}`);
      } catch (e) { }
    }
  };

  return (
    <div className="row">
      <div className="col s8 offset-s2" style={{ padding: '2rem' }}>
        <div className="input-field">
          <input type="text" name="link" id="link" placeholder="Enter link for short" value={link} onChange={ e => setLink(e.target.value) } onKeyPress={pressHandler} />
          <label htmlFor="link">Link</label>
        </div>
      </div>
    </div>
  );
};

export default CreatePage;