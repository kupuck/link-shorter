import { useCallback, useState, useEffect } from 'react';


const useAuth = () => {
  const storageName = 'userData';
  const [token, setToken] = useState(null);
  const [ready, setReady] = useState(false);
  const [userId, setUserId] = useState(null);

  const signIn = useCallback((jwtToken, id) => {
    setToken(jwtToken);
    setUserId(id);
    localStorage.setItem(storageName, JSON.stringify({ userId: id, token: jwtToken }));
  }, []);

  const signOut = useCallback(() => {
    setToken(null);
    setUserId(null);
    localStorage.removeItem(storageName);
  }, []);

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem(storageName));
    if (data && data.token) {
      signIn(data.token, data.userId);
    }
    setReady(true);
  }, [signIn]);

  return { signIn, signOut, token, userId, ready };
};

export default useAuth;