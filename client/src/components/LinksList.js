import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const LinksList = ({ links }) => {
  if (!links.length) {
    return <p className="center">There are no links</p>
  }
  return (
    <Fragment>
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Origin</th>
            <th>Shorted</th>
            <th>Open</th>
          </tr>
        </thead>
        <tbody>
          { links.map((link, key) => {
            return (
              <tr key={ link._id }>
                <td>{ key + 1 }</td>
                <td>{ link.from }</td>
                <td>{ link.to }</td>
                <td><Link to={`/detail/${link._id}`}>Open</Link></td>
              </tr>
            );
          }) }
        </tbody>
      </table>
    </Fragment>
  );
};

export default LinksList;