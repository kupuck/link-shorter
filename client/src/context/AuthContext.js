import { createContext } from 'react';

function noop() {}

const AuthContext = createContext({
  token: null,
  userId: null,
  signIn: noop,
  signOut: noop,
  isAuthenticated: false
});

export default AuthContext;